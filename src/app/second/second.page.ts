import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-second',
  templateUrl: './second.page.html',
  styleUrls: ['./second.page.scss'],
})
export class SecondPage implements OnInit {
  public items: string[] = [];

  constructor() {}

  ngOnInit() {
    this.items = [
      'just',
      'a',
      'few',
      'elements',
      'to',
      'start',
      'with',
      'then',
      'we',
      'will',
      'load',
      'some',
      'more',
    ];
  }

  doInfinite(event) {
    const newItems = new Array(100).fill('more items!');
    this.items.push(...newItems);

    event.target.complete();
  }
}
