import {
  Component,
  OnInit,
  AfterViewInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { IonItem } from '@ionic/angular';

import { DataService } from '../services/data.service';

@Component({
  selector: 'app-fourth',
  templateUrl: './fourth.page.html',
  styleUrls: ['./fourth.page.scss'],
})
export class FourthPage implements OnInit, AfterViewInit {
  @ViewChildren(IonItem) itemRefs: QueryList<IonItem>;
  public items = [];
  private myLocalNodes = [];

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.items = new Array(30);
  }

  ngAfterViewInit() {
    // Will create detached DOM nodes
    this.itemRefs.forEach((item) => {
      this.dataService.myNodes.push(item);
    });

    // Will NOT create detached DOM Nodes
    // this.itemRefs.forEach((item) => {
    //   this.myLocalNodes.push(item);
    // });

    console.log(this.dataService.myNodes);
  }
}
