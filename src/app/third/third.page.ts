import { Component } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-third',
  templateUrl: './third.page.html',
  styleUrls: ['./third.page.scss'],
})
export class ThirdPage {
  items: any[] = [];

  constructor(public dataService: DataService) {}

  ionViewDidEnter() {
    console.log('creating interval');

    setInterval(() => {
      console.log('pushing lots of elements');

      for (let i = 0; i < 100000; i++) {
        this.items.push('uh oh');
      }
    }, 1000);

    // const bigArray = [];

    // for (let i = 0; i < 1000; i++) {
    //   bigArray.push(document.createElement('div'));
    // }

    // this.dataService.myData.push.apply(this.dataService.myData, bigArray);

    // console.log('Array length: ', this.dataService.myData.length);
  }
}
